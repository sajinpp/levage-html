<div class="inear_header">
    <?php include "master/header.php" ?>
</div>
<main>
    <div class="about_page">
        <div class="about_strokeimg">
            <img src="assets/images/about_image1.png" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="about_company">
                        <div class="about_head">
                            
                            <div class="stroke_wrapper">
                                <div class="stroke">
                                    <h2>
                                        About Us
                                    </h2>
                                    <img src="assets/images/About_stroke.png" alt="">
                                </div>
    
                                <p class="stroke_p">
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                                <p class="paragraph2">
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>

                                <div class="hidden_image">
                                    <img src="assets/images/about_image1.png" alt="">
                                </div>

                            </div>

                            <div class="content">
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>

                                <div class="about_image2">
                                    <img src="assets/images/about_image2.png" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="ceo_section">
                        <div class="ceo_image">
                            <img src="assets/images/ceo_image.png" alt="">
                        </div>
                        <div class="ceo_details">
                            <h2>
                                CEO Talks
                            </h2>
                            <p>
                                Gravida nibh arcu sit donec. Venenatis odio sed elementum quis tristique scelerisque molestie quam risus. Egestas aliquet semper libero quam vitae ut. Id sit enim est at duis diam. Id lectus natoque risus ultrices aliquam mauris consectetur. Hac eu mollis elementum at aenean amet. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam.
                            </p>
                            <h4>
                                Jon Doe
                            </h4>
                            <h5>
                                CEO Levage
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="why_choose_section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="why_choose">
                            <div class="why_chooseContent">
                                <h2>
                                    Why Choose us
                                </h2>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                               <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                               </p>
                            </div>
                            <div class="why_chooseImg">
                                <img src="assets/images/about_image2.png" alt="">
                            </div>
                        </div>
                        <div class="whychoose_description">
                            <div class="whychooseimg2">
                                <img src="assets/images/whychooseimg2.png" alt="">
                            </div>
                            <div class="whychoose_content2">
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores.Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores.  
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="vision_section">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="vision_wrapper">
                            <div class="vision_content">
                                <h3>
                                    Our Visions
                                </h3>
                                <p>
                                    Gravida nibh arcu sit donec. Venenatis odio sed elementum quis tristique scelerisque molestie quam risus. Egestas aliquet semper libero quam vitae ut. Id sit enim est at duis diam. Id lectus natoque risus ultrices aliquam mauris consectetur. Hac eu mollis elementum at aenean amet. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam.
                                </p>
                            </div>
                            <div class="vision_img">
                                <img src="assets/images/visions.svg" alt="">
                            </div>
                        </div>
                        <div class="mission_wrapper">
                            <div class="vision_img">
                                <img src="assets/images/mission.svg" alt="">
                            </div>
                            <div class="vision_content">
                                <h3>
                                    Our Missions
                                </h3>
                                <p>
                                    Gravida nibh arcu sit donec. Venenatis odio sed elementum quis tristique scelerisque molestie quam risus. Egestas aliquet semper libero quam vitae ut. Id sit enim est at duis diam. Id lectus natoque risus ultrices aliquam mauris consectetur. Hac eu mollis elementum at aenean amet. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam.
                                </p>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </section>



    </div>
</main>

<?php include 'master/footer.php' ?>