<div class="inear_header">
    <?php include "master/header.php" ?>
</div>
<main>
    <div class="gallery_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gallery_head"></div>
                    <div class="gallery_wrapper">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                              <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Image</button>
                            </li>
                            <li class="nav-item" role="presentation">
                              <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Video</button>
                            </li>
                          </ul>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                <nav class="inear_tabs">
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                      <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Truck Mounted Telescoping Boom Cranes</button>
                                      <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Truck Mounted Telescoping Knuckle Boom Cranes</button>
                                      <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Marine Cranes</button>
                                      
                                    </div>
                                  </nav>
                                  <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" tabindex="0">
                                      <div class="gallery_imagewrapper">
                                        <div class="gallery_row1">
                                          <div class="gallery_column">
                                            <div class="gallery_firstbox common_box">
                                              <img src="assets/images/gallery1.png" alt="">
                                              <img src="assets/images/gallery2.png" alt="">
                                            </div>
                                            <div class="gallery_secondbox common_box">
                                              <img src="assets/images/gallery3.png" alt="">
                                            </div>
                                          </div>
                                          <div class="gallery_column2 ">
                                            <div class="gallery_firstbox">
                                              <img src="assets/images/gallery4.png" alt="">
                                            </div>
                                            <div class="gallery_secondbox">
                                              <img src="assets/images/gallery5.png" alt="">
                                              <img src="assets/images/gallery6.png" alt="">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="gallery_row2">
                                          <div class="gallery_column second_row">
                                            <div class="gallery_firstbox common_box">
                                              <img src="assets/images/gallery1.png" alt="">
                                              <img src="assets/images/gallery2.png" alt="">
                                            </div>
                                          </div>
                                          <div class="gallery_column2 ">
                                            <div class="gallery_firstbox">
                                              <img src="assets/images/gallery4.png" alt="">
                                            </div>
                                            <div class="gallery_secondbox">
                                              <img src="assets/images/gallery5.png" alt="">
                                              <img src="assets/images/gallery6.png" alt="">
                                            </div>
                                          </div>
                                          <div class="gallery_secondbox common_box secondrow_lastimg">
                                            <img src="assets/images/gallery3.png" alt="">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab" tabindex="0">...</div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab" tabindex="0">...</div>
                                    
                                  </div>
                            </div>
                            <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                <ul class=" video_tabs nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                      <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">Truck Mounted Telescoping Boom Cranes</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                      <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Truck Mounted Telescoping Knuckle Boom Cranes</button>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                      <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Marine Cranes</button>
                                    </li>

                                    
                                   
                                  </ul>
                                  <div class="tab-content" id="pills-tabContent ">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                                        <ul class="gallery_boxwrapper">
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="gallery_video">
                                                        <video  
                                                        class="featured_banner_video bannerVideo w-full h-full object-cover"
                                                        playsinline
                                                        muted
                                                       
                                                        loop
                                                        width="100%"
                                                      >
                                                        <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                      </video>
                                                      <div class="youtube_btn">
                                                        <img src="assets/images/videopause.svg" alt="">
                                                      </div>
                                                    </div>
                                                </li>
                                            </ul>
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0">...</div>
                                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" tabindex="0">...</div>
                                   
                                  </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>