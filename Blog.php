<div class="inear_header">
    <?php include "master/header.php" ?>
</div>

<main>
    <div class="blog_Page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_wrapper">
                        <div class="contact_head">
                            <div class="contact_stroke">
                                <h2>
                                    Blog
                                </h2>
                                <img src="assets/images/Blog_stroke.png" alt="">

                                <div class="vector_truck">
                                    <img src="assets/images/Vector_truck.png" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. 
                            </p>
                        </div>


                        <ul class="blog_list certificate_wrapper">
                            <li>
                               <a href="blogDetails.php">
                                <div class="blog_image">
                                    <img src="assets/images/blog2.png" alt="">
                                </div>
                                <div class="blog_contents">
                                    <h6>29 Feb 2024</h6>
                                    <h2>Levage launched sato cranes service</h2>
                                    <p class="content">
                                        When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                        When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                    </p>
                                    <span class="read_more">Read More</span>
                                   
                                </div>
                               </a>
                            </li>
                            <li>
                               <a href="blogDetails.php">
                                <div class="blog_image">
                                    <img src="assets/images/blog3.png" alt="">
                                </div>
                                <div class="blog_contents">
                                    <h6>29 Feb 2024</h6>
                                    <h2>Levage launched sato cranes service</h2>
                                    <p class="content">
                                        When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                        When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                    </p>
                                    <span class="read_more">Read More</span>
                                </div>
                               </a>
                            </li>
                            <li>
                                <a href="blogDetails.php">
                                    <div class="blog_image">
                                        <img src="assets/images/blog1.png" alt="">
                                    </div>
                                    <div class="blog_contents">
                                        <h6>29 Feb 2024</h6>
                                        <h2>Levage launched sato cranes service</h2>
                                        <p class="content">
                                            When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                            When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                        </p>
                                        <span class="read_more">Read More</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="blogDetails.php">
                                 <div class="blog_image">
                                     <img src="assets/images/blog1.png" alt="">
                                 </div>
                                 <div class="blog_contents">
                                     <h6>29 Feb 2024</h6>
                                     <h2>Levage launched sato cranes service</h2>
                                     <p class="content">
                                         When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                         When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                     </p>
                                     <span class="read_more">Read More</span>
                                    
                                 </div>
                                </a>
                             </li>
                             <li>
                                <a href="blogDetails.php">
                                 <div class="blog_image">
                                     <img src="assets/images/blog3.png" alt="">
                                 </div>
                                 <div class="blog_contents">
                                     <h6>29 Feb 2024</h6>
                                     <h2>Levage launched sato cranes service</h2>
                                     <p class="content">
                                         When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                         When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                     </p>
                                     <span class="read_more">Read More</span>
                                 </div>
                                </a>
                             </li>
                             <li>
                                 <a href="blogDetails.php">
                                     <div class="blog_image">
                                         <img src="assets/images/blog2.png" alt="">
                                     </div>
                                     <div class="blog_contents">
                                         <h6>29 Feb 2024</h6>
                                         <h2>Levage launched sato cranes service</h2>
                                         <p class="content">
                                             When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                             When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.
                                         </p>
                                         <span class="read_more">Read More</span>
                                     </div>
                                 </a>
                             </li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="map_wrapper">
            <img src="assets/images/map_img.png" alt="">
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>