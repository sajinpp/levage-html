<?php include "master/header.php" ?>

<main>
    <section class="banner_section">
        <div class="container banner_container">
            <div class="row">
                <div class="col-12">
                    <div class="banner_slideWrapper">
                        <div class="swiper banner_swiper">
                          <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="banner_wrapper">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="banner_contents">
                                                    <h1>
                                                        Experience Efficiency with All Product
                                                    </h1>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                                    </p>
                                                    <div class="banner_framevideo">
                                                        <video class="featured_banner_video bannerVideo w-full h-full object-cover" playsinline
                                                            muted
                                                        
                                                            loop
                                                            width="100%"
                                                        >
                                                            <source src="assets/images/truck_video.mp4" type="video/mp4" />
                                                        </video>
                                                        <div class="youtube_btn">
                                                            <img src="assets/images/icons/pause.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                 </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="banner_wrapper">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="banner_contents">
                                                    <h1>
                                                        Experience Efficiency with All Product
                                                    </h1>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                                    </p>
                                                    <div class="banner_framevideo">
                                                        <video class="featured_banner_video bannerVideo w-full h-full object-cover" playsinline
                                                            muted
                                                        
                                                            loop
                                                            width="100%"
                                                        >
                                                            <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                        </video>
                                                        <div class="youtube_btn">
                                                            <img src="assets/images/icons/pause.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                 </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="banner_wrapper">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="banner_contents">
                                                    <h1>
                                                        Experience Efficiency with All Product
                                                    </h1>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                                    </p>
                                                    <div class="banner_framevideo">
                                                        <video class="featured_banner_video bannerVideo w-full h-full object-cover" playsinline
                                                            muted
                                                        
                                                            loop
                                                            width="100%"
                                                        >
                                                            <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                        </video>
                                                        <div class="youtube_btn">
                                                            <img src="assets/images/icons/pause.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                 </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="banner_wrapper">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="banner_contents">
                                                    <h1>
                                                        Experience Efficiency with All Product
                                                    </h1>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                                    </p>
                                                    <div class="banner_framevideo">
                                                        <video class="featured_banner_video bannerVideo w-full h-full object-cover" playsinline
                                                            muted
                                                        
                                                            loop
                                                            width="100%"
                                                        >
                                                            <source src="assets/images/bannerVideo.mp4" type="video/mp4" />
                                                        </video>
                                                        <div class="youtube_btn">
                                                            <img src="assets/images/icons/pause.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                 </div>
                            </div>
                          </div>
                          <div class="container banner_dotContainer">
                            <div class="swiper-pagination" id="banner_dots"></div>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about_section">
        <div class="container">
            <div class="row">
                <div class="col-12 about_wrapper">
                    <div class="about_banner">
                        <h6 class="sub_heading">
                            About Us
                        </h6>
                        <h2 class="heading">
                            We Experienced in This Field
                        </h2>

                        <div class="about_img">
                            <img src="assets/images/about_banner.png" alt="">
                        </div>

                    </div>
                    <div class="about_contents">
                        <div class="masking_image">
                            <video width="100%" height="100%" loop muted autoplay>
                                <source src="assets/images/truck_video.mp4" type="video/mp4">
                              </video>
                        </div>
                        <p>
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. also the leap into electronic typesetting.
                        </p>
                        <a href="" class="primary_btn">
                           <span> Know More</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="capacity_range">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="capacity_wrapper">
                        <div class="capacity_head">
                            <div class="content">
                               <div class="head_tag">
                                <h2>
                                    Capacity Range
                                </h2>
                                <img src="assets/images/Capacity_stroke.svg" alt="">
                               </div>
                                <p>
                                    Sed risus magna porta elit metus nullam cursus mattis lectus. Viverra mauris sed viverra id aenean. Lorem magna libero vulputa
                                </p>
                            </div>
                            <div class="view_more">
                                <a class="primary_btn" href="">
                                    <span>View More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include 'master/footer.php' ?>