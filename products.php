<div class="inear_header">
    <?php include "master/header.php" ?>
</div>

<main>
    <div class="products_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_wrapper">
                        <div class="contact_head">
                            <div class="contact_stroke">
                                <h2>
                                    Capacity Range
                                </h2>
                                <img src="assets/images/products_stroke.png" alt="">
                            </div>
                            <p>
                                Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. 
                            </p>
                        </div>

                        <ul class="certificate_wrapper">
                            <li>
                               <div class="client_wrapper">
                                <div class="client_img">
                                    <img src="assets/images/testimonials_img.png" alt="">
                                </div>
                                <div class="client_details">
                                    <h6>
                                        James Chako
                                    </h6>
                                    <h5>
                                        Lead Technician
                                    </h5>
                                </div>
                               </div>
                                <p>
                                Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                               </p>
                            </li>
                            <li>
                                <div class="client_wrapper">
                                 <div class="client_img">
                                     <img src="assets/images/testimonials_img.png" alt="">
                                 </div>
                                 <div class="client_details">
                                     <h6>
                                         James Chako
                                     </h6>
                                     <h5>
                                         Lead Technician
                                     </h5>
                                 </div>
                                </div>
                                <p>
                                 Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                             </li>
                             <li>
                                <div class="client_wrapper">
                                 <div class="client_img">
                                     <img src="assets/images/testimonials_img.png" alt="">
                                 </div>
                                 <div class="client_details">
                                     <h6>
                                         James Chako
                                     </h6>
                                     <h5>
                                         Lead Technician
                                     </h5>
                                 </div>
                                </div>
                                <p>
                                 Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                             </li>
                             <li>
                                <div class="client_wrapper">
                                 <div class="client_img">
                                     <img src="assets/images/testimonials_img.png" alt="">
                                 </div>
                                 <div class="client_details">
                                     <h6>
                                         James Chako
                                     </h6>
                                     <h5>
                                         Lead Technician
                                     </h5>
                                 </div>
                                </div>
                                <p>
                                 Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                             </li>
                             <li>
                                <div class="client_wrapper">
                                 <div class="client_img">
                                     <img src="assets/images/testimonials_img.png" alt="">
                                 </div>
                                 <div class="client_details">
                                     <h6>
                                         James Chako
                                     </h6>
                                     <h5>
                                         Lead Technician
                                     </h5>
                                 </div>
                                </div>
                                <p>
                                 Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                             </li>
                             <li>
                                <div class="client_wrapper">
                                 <div class="client_img">
                                     <img src="assets/images/testimonials_img.png" alt="">
                                 </div>
                                 <div class="client_details">
                                     <h6>
                                         James Chako
                                     </h6>
                                     <h5>
                                         Lead Technician
                                     </h5>
                                 </div>
                                </div>
                                <p>
                                 Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. 
                                </p>
                             </li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="map_wrapper">
            <img src="assets/images/map_img.png" alt="">
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>