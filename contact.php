<div class="inear_header">
    <?php include "master/header.php" ?>
</div>

<main>
    <div class="contact_Page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_wrapper">
                        <div class="contact_head">
                            <div class="contact_stroke">
                                <h2>
                                     Contact us
                                </h2>
                                <img src="assets/images/contact_stroke.png" alt="">
                            </div>
                            <p>
                                Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. 
                            </p>
                        </div>
                        <div class="contct_formwrapper">
                            <div class="contact_form_img">
                                <img src="assets/images/contactform_img.png" alt="">
                            </div>
                            <div class="contact_form">
                                <form action="">
                                    <h3>
                                        Get in touch with us
                                    </h3>
                                    <p>
                                        The hotel’s signature profile is that of an Art hotel, inspired by and integrated with nature. 
                                    </p>
                                    <div class="iput_group">
                                        <div class="input_parent">
                                            <input type="text">
                                        </div>
                                        <div class="multi_inputs">
                                            <div class="input_parent">
                                            <input type="text">
                                        </div>
                                        <div class="input_parent">
                                            <input type="text">
                                        </div>
                                        </div>
                                        <div class="input_parent">
                                            <textarea name="" id=""></textarea>
                                        </div>
                                    </div>
                                    <div class="terms_policy">
                                        <input type="checkbox">
                                        <p>
                                            I have read and understand the terms
                                        </p>
                                    </div>
                                    <button class="primary_btn">
                                            <span>
                                                Enquire Now
                                            </span>
                                        </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map_wrapper">
            <img src="assets/images/map_img.png" alt="">
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>