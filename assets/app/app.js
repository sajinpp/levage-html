
// document.addEventListener("DOMContentLoaded", function() {
//   var readMoreLinks = document.querySelectorAll(".read_more");
  
//   readMoreLinks.forEach(function(link) {
//       link.addEventListener("click", function(event) {
//           event.preventDefault();
          
//           var content = this.parentNode.querySelector(".content");
          
//           if (content.classList.contains("hide-text")) {
//               content.classList.remove("hide-text");
//               this.textContent = "Read Less";
//           } else {
//               content.classList.add("hide-text");
//               this.textContent = "Read More";
//           }
//       });
//   });
// });



// Selecting readmore elements and corresponding content
let readmore = document.querySelectorAll(".read_more");
let content = document.querySelectorAll(".blog_list li p");

// Function to handle read more click event
function readmoreFunction(event) {
    // Toggle class for readmore element
    event.target.classList.toggle("active");

    // Find the index of the clicked readmore element
    let index = Array.from(readmore).indexOf(event.target);

    // Toggle class for corresponding content
    if (content[index].classList.contains("show")) {
        content[index].classList.remove("show");
        event.target.textContent = "Read More";  // Change text to "Read More"
    } else {
        content[index].classList.add("show");
        event.target.textContent = "Read Less";  // Change text to "Read Less"
    }
}

// Adding click event listener to each readmore element
readmore.forEach(item => {
    item.addEventListener("click", readmoreFunction);
});




$(document).ready(function () {
    var banner_swiper = new Swiper('.banner_swiper', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 20,
        speed: 900,
        // autoplay: {
        //     delay: 2500,
        //     disableOnInteraction: false,
        // },
        pagination: {
            el: "#banner_dots",
            clickable: true,
        },
    });


      //featured_banner video play

  document.querySelectorAll(".bannerVideo").forEach(function (video) {
    video.parentElement.addEventListener("click", function () {
      if (video.paused) {
        video.play();
        this.querySelector(".youtube_btn").style.display = "none";
      } else {
        video.pause();
        this.querySelector(".youtube_btn").style.display = "block";
      }
    });
  });
 

});



  
  
  
  
 