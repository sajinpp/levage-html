<div class="inear_header">
    <?php include "master/header.php" ?>
</div>

<main>
    <div class="certificate_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_wrapper">
                        <div class="contact_head">
                            <div class="contact_stroke">
                                <h2>
                                    Certifications
                                </h2>
                                <img src="assets/images/Certifications_stroke.png" alt="">
                            </div>
                            <p>
                                Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel. Sunt est qui ab non dolores repellat rem impedit dolores. Ut ea rerum cum eum. Alias dolores tempore illo accusantium est et. 
                            </p>
                        </div>

                        <ul class="certificate_wrapper">
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate1.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate2.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate1.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate1.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate2.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                            <li>
                                <div class="certificate_img">
                                    <img src="assets/images/certificate1.png" alt="">
                                </div>
                                <h6>
                                    ISO 9001:2015
                                </h6>
                                <p>
                                    Lorem ipsm molestiae delectus nemo alias nesciunt harum et. Nobis dolorum excepturi quod vel.
                                </p>
                            </li>
                        </ul>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="map_wrapper">
            <img src="assets/images/map_img.png" alt="">
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>